-- chikun :: 2015
-- Lua text based engine - LOVE2D work


require "src/printFormat"       -- Import the printFormat


function love.load()

    timer = 0

    scroll = 0              -- Scroll value of the text

    startBleep = love.audio.newSource('ogg/startup.ogg', 'static')

    splashFnt = love.graphics.newFont('fnt/exo2.otf', 96)

    menuButton = {
        x = love.graphics.getWidth() - 32,
        y = 0,
        w = 32,
        h = 32
    }

    gfx = {
        cog = love.graphics.newImage("gfx/cog.png")
    }

    menu = require "src/menu"

end


function love.update(dt)


    timer = math.min(1.5, timer + dt)

    if menuOpen then

        menu:update(dt)

    elseif timer >= 1 then

        if not gameLoaded then

            gameLoaded = true

            loadGame()

        end

        audio.rainOutside:setVolume((timer - 1) * 2)

        -- Make cursor blink
        cursorTimer = (cursorTimer + dt * 1.5) % 2

        if love.keyboard.isDown("backspace") then

            backspaceTimer = backspaceTimer + dt

            if backspaceTimer > 0.4 then

                doBackspace()

                backspaceTimer = backspaceTimer - 0.04

            end
        end

    elseif timer >= 0.5 and not soundPlayed then

        soundPlayed = true

        startBleep:play()

    end


end


function love.draw()

    if timer >= 1 then

        love.graphics.setBackgroundColor(cols[6])

        love.graphics.setFont(fnt)

        local wrap = { fnt:getWrap(text, 790) }

        local textHeight = (wrap[2] * fnt:getHeight()) +
            (wrap[2] - 1) * fnt:getLineHeight()

        if fnt == fntList.terminus then

            textHeight = textHeight - fnt:getHeight()

        end

        printFormat(text, 16, 430 - textHeight + scroll, 790)

        love.graphics.setColor(cols[6])

        love.graphics.rectangle('fill', 0, 438, 854, 42)

        love.graphics.setColor(cols[5])

        love.graphics.line(16, 438, 838, 438)

        local cursor = " "

        if math.floor(cursorTimer) == 0 and not menuOpen then cursor = "_" end

        love.graphics.print("> ", 16, 446)

        love.graphics.setColor(cols[5])

        love.graphics.print(input .. cursor, 32, 446)

        love.graphics.setColor(cols[5])

        if math.overlap({ x = love.mouse.getX(), y = love.mouse.getY(),
                w = 1, h = 1 }, menuButton) then
            love.graphics.setColor(cols[1])
        end

        love.graphics.draw(gfx.cog, menuButton.x, menuButton.y)

    end

    if timer < 1.5 then

        fade = math.min(timer * 2, 1 - (timer - 1) * 2, 1)

        love.graphics.setFont(splashFnt)

        love.graphics.setColor(0, 0, 0, fade * 255)

        love.graphics.rectangle("fill", 0, 0, 854, 480)

        love.graphics.setColor(255, 255, 255, fade * 255)

        love.graphics.printf("chikun", 427, 192, 0, 'center')

    end

    if menuOpen then

        love.graphics.setColor(0, 0, 0, 128)

        love.graphics.rectangle('fill', 0, 0, love.graphics.getWidth(),
            love.graphics.getHeight())

        menu:draw()

    end

end


function love.textinput(char)

    if text and input and not menuOpen then

        if input:len() < 32 then

            input = input .. char

        end

        cursorTimer = 0

    end
end


function love.keypressed(key)

    if text and input and not menuOpen then

        if key == "return" then

            text = text .. "\n> col=5 " .. input .. " col=1"

            if input:len() > 0 then

                runInput(input)

            end

            input = ""

        elseif key == "backspace" then

            backspaceTimer = 0

            doBackspace()

        end
    end

    if menuOpen then

        menu:onKey(key)

    end

end


function love.mousepressed(x, y, mb)

    if timer < 1.5 then return 0 end

    cursor = {
        x = love.mouse.getX(),
        y = love.mouse.getY(),
        w = 1 ; h = 1
    }

    local wrap = { fnt:getWrap(text, 790) }

    local textHeight = (wrap[2] * fnt:getHeight()) +
        (wrap[2] - 1) * fnt:getLineHeight()

    if fnt == fntList.terminus then

        textHeight = textHeight - fnt:getHeight()

    end

    if menuOpen then

        menu:onClick(x, y, mb)

    else

        if mb == "wu" then

            local size = math.max(0, textHeight - 414)

            scroll = math.min(size, scroll + 48)

        elseif mb == "wd" then

            scroll = math.max(0, scroll - 48)

        end
    end

    if mb == "l" then

        if math.overlap(cursor, menuButton) then
            menuOpen = not menuOpen
        end

    end

end


function doBackspace()

    if input:len() > 1 then

        input = input:sub(1, -2)

        cursorTimer = 0

    else

        input = ""

    end

end



function loadGame()

    -- The text log
    text = "col=2 LEFTBRIDGE MANSION col=1\n" ..
        "col=4 by Jane Slade & Josef Frank col=1\n" ..
        "---------------------------"

    -- Typed input from user
    input = ""

    -- Keeps track of the cursor blinking
    cursorTimer = 0

    -- Keeps track of backspace repeating
    backspaceTimer = 0

    -- Set font to a newly loaded one
    fntList = {
        terminus =  love.graphics.setNewFont("fnt/terminus.ttf", 18),
        november =  love.graphics.setNewFont("fnt/novem___.ttf", 18),
        robotoslab = love.graphics.setNewFont("fnt/robotoSlab.ttf", 18)
    }

    fnt = fntList.november

    -- The push function simply adds to 'text', with a newline beforehand
    push = function(output) text = text .. "\n" .. output end

    require "src/game"      -- Load up game variables and functions

    colSchemes = {

        default = {
            { 192, 192, 192 },      -- FG fade
            { 230, 20, 20 },        -- Red
            { 0, 230, 130 },        -- Greenish
            { 255, 128, 0 },        -- Orange
            { 255, 255, 255 },      -- FG
            { 0, 0, 0 }             -- BG
        },
        inverted = {
            { 64, 64, 64 },         -- White
            { 20, 230, 230 },       -- Red
            { 255, 20, 120 },       -- Greenish
            { 0, 128, 255 },        -- Orange
            { 0, 0, 0 },            -- FG
            { 255, 255, 255 }       -- BG
        },
        matrix = {
            { 4, 219, 19 },         -- White
            { 4, 219, 19 },         -- Red
            { 4, 219, 19 },         -- Greenish
            { 4, 219, 19 },         -- Orange
            { 4, 219, 19 },         -- FG
            { 0, 0, 0 }             -- BG
        },
        solar = {
            { 147, 161, 161 },         -- White
            { 220, 50, 47 },         -- Red
            { 133, 153, 0 },         -- Greenish
            { 203, 75, 22 },         -- Orange
            { 253, 246, 227 },         -- FG
            { 0, 43, 54 }             -- BG
        },
        choc = {
            { 87, 65, 47 },         -- White
            { 106, 26, 74 },         -- Red
            { 78, 105, 26 },         -- Greenish
            { 24, 61, 97 },         -- Orange
            { 90, 39, 41 },         -- FG
            { 213, 196, 161 }             -- BG
        },
        hotdog = {
            { 147, 161, 161 },         -- White
            { 220, 50, 47 },         -- Red
            { 133, 153, 0 },         -- Greenish
            { 203, 75, 22 },         -- Orange
            { 253, 246, 227 },         -- FG
            { 0, 43, 54 }             -- BG
        }

    }

    -- All text colours from the game
    cols = colSchemes.default

    menu:start()

end


-- Checks if two rectangle objects overlap
function math.overlap(obj1, obj2)

    return (obj1.x < obj2.x + obj2.w and
            obj2.x < obj1.x + obj1.w and
            obj1.y < obj2.y + obj2.h and
            obj2.y < obj1.y + obj1.h)

end
