-- 2nd Hallway

local secondHallway = {
    visited = false,
    items = { }
}


-- Get house description
function secondHallway.onLook(fullDescription)

    local description = "A long dimly lit hallway. The floorboards shift slightly"..
    "as you walk upon them. There are rooms to the North, North East, and North West."

    if guardian then

        push("A spidery machine is coming towards you.")

    else

        if fullDescription or not secondHallway.visited then

            secondHallway.visited = true

            description = "A long dimly lit hallway. The floorboards shift slightly as"..
        "you walk upon them. There are rooms to the North, North East, and North West."

        end

    end

    return description

end


-- Get north path

function secondHallway.getSouth()

    return "upstairsFoyer"

end

function secondHallway.getNorth()

    return "study"

end

function secondHallway.getNorthEast()

    return "guestRoom2"

end

function secondHallway.getNorthWest()

    return "gamesRoom"

end

return secondHallway
