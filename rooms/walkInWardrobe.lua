-- Master Bedroom

local walkInWardrobe = {
    visited = false,
    items = { }
}


-- Get house description
function walkInWardrobe.onLook(fullDescription)

    local description =  "It is filled with moth eaten coats, and musty old shoes."..
    "The Master Bedroom lies to your East."

    if fullDescription or not rooms.walkInWardrobe.visited then

        rooms.walkInWardrobe.visited = true

        description = "You are standing in a cramped room smelling over poweringly."..
    "of moth balls. It is filled with moth eaten coats, and musty old shoes."..
        "The Master Bedroom lies to your East."

    end

    return description

end


-- Get north path
function walkInWardrobe.getEast()

    return "masterBedroom"

end

return walkInWardrobe
