local chamber = {
    visited = false,
    items = { }
}


function chamber.onLook(fullDescription)

    local description = "You are standing in a large chamber."

    return description

end

function chamber.getSouth()

    return "kitchen"

end

function chamber.getEast()

    return "sittingRoom"

end

return chamber
