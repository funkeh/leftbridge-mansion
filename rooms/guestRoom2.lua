-- Guest Room Upstairs

local guestRoom2 = {
    visited = false,
    items = {
        coat = "tornJacket"
    }
}


-- Get room description

function guestRoom2.onLook(fullDescription)

    local description = "A small bedroom, consisting of "..
            "only a bed and two drawers. "..
            "There is a coat on the floor."

    return description

end


-- Get north path
function guestRoom2.getSouth()

    return "secondHallway"

end


return guestRoom2
