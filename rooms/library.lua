
local library = {
    visited = false,
    items = {
    }
}


-- Get house description
function library.onLook(fullDescription)

    local description = "You are standing in a library."

    if fullDescription or not rooms.library.visited then

        rooms.library.visited = true

        description = "LIBRARY"

    end

    return description

end


-- Get

function library.getDown()

    return "secondHallway"

end

return library
