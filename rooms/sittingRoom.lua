-- Sitting Room

local sittingRoom = {
    visited = false,
    items = {
    }
}


-- Get description
function sittingRoom.onLook(fullDescription)

    local description = "You are standing in a sitting room."

    if fullDescription or not rooms.sittingRoom.visited then

        rooms.sittingRoom.visited = true

        description = "sittingRoom"

    end

    return description

end


-- Get

function sittingRoom.getSouth()

    return "foyer"

end

function sittingRoom.getWest()

    return "chamber"

end

function sittingRoom.getEast()

    return "hallway"

end

return sittingRoom
