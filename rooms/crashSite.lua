-- chikun :: 2015
-- Crash site (outside)


local crashSite = {
    visited = false,
    items = {
        cliff = true,
        car = true,
        backpack = true,
    }
}


-- Get house description
function crashSite.onLook(fullDescription)

    local description = "You are at the crash site. The car is a write-off."

    if fullDescription or not rooms.crashSite.visited then

        rooms.crashSite.visited = true

        description = "Ouch! You were in a col=3 car col=1 crash and banged your head up pretty good. " ..
        "Thankfully, your compass still works and you see a col=3 path col=1 to the north."

    end

    return description

end


-- Get north path
function crashSite.getNorth()

    return "forestPath"

end


return crashSite
