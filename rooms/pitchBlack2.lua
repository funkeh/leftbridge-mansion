-- Pitch Black 2

local pitchBlack2 = {
    counter = 0
}

function pitchBlack2.onLook(fullDescription)

    return "You stumble around in the dark. From somewhere close to you, you hear "..
    "the clattering of claws against the floor boards and the heavy breathe of "..
    "some animal. Get Out Now!"

end

-- If one doesn't leave the room before the counter gets to one,
-- they are killed by the totally not Grue

function pitchBlack2.notLeaving()

    if rooms.pitchBlack2.counter == 0 then
        push("You feel like you are running around in circles.")
    else
        push("You are torn to shreds.")
    end

    rooms.pitchBlack2.counter = rooms.pitchBlack2.counter + 1

end

pitchBlack2.getNorth = pitchBlack2.notLeaving
pitchBlack2.getEast  = pitchBlack2.notLeaving
pitchBlack2.getWest  = pitchBlack2.notLeaving
pitchBlack2.getNorthEast  = pitchBlack2.notLeaving
pitchBlack2.getSouthEast  = pitchBlack2.notLeaving
pitchBlack2.getNorthWest = pitchBlack2.notLeaving
pitchBlack2.getSouthWest  = pitchBlack2.notLeaving



function pitchBlack2.getSouth()

    return "foyer"

end

function pitchBlack2.getWest()

    return "foyer"

end

return pitchBlack2
