-- Master Bedroom



local masterBedroom = {
    visited = false,
    items = { }
}


-- Get house description
function masterBedroom.onLook(fullDescription)

    local description= "You are standing in a large room with a perfectly made"..
    "four poster bed. There is a painting on the wall, and a chest of drawers to"..
    "the East."..
    "There's a light to your North, and a walk-in room to your East."

    if fullDescription or not masterBedroom.visited then

        masterBedroom.visited = true

        description =  "You are standing in a large room with a perfectly made"..
        "four poster bed. There is a painting on the wall, and a chest of drawers to"..
        "the East."..
        "There's a light to your North, and a walk-in room to your West."

    end

    return description

end


-- Get north path
function masterBedroom.getNorth()

    return "upstairsFoyer"

end

function masterBedroom.getWest()

    return "walkInWardrobe"

end

return masterBedroom
