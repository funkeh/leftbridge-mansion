-- Guest Room

local guestRoom1 = {
    visited = false
}

function guestRoom1.onLook(fullDescription)

    description = "guestroom"

    return description

end

-- To get back to Hallway

function guestRoom1.getWest()

    return "hallway"

end

return guestRoom1
