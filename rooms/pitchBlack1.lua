-- Pitch Black 1

local pitchBlack1 = {
    counter = 0
}

function pitchBlack1.onLook()

    return "You stumble around in the dark. From somewhere close to you, you hear "..
    "the clattering of claws against the floor boards and the heavy breathe of "..
    "some animal. Get Out Now!"

end

-- If one doesn't leave the room before the counter gets to one,
-- they are killed by the totally not Grue

function pitchBlack1.notLeaving()

    if rooms.pitchBlack1.counter == 0 then
        push("You feel like you are running around in circles.")
    else
        push("You are torn to shreds.")
    end

    rooms.pitchBlack1.counter = rooms.pitchBlack1.counter + 1

end

pitchBlack1.getNorth = pitchBlack1.notLeaving
pitchBlack1.getEast  = pitchBlack1.notLeaving
pitchBlack1.getWest  = pitchBlack1.notLeaving
pitchBlack1.getNorthEast  = pitchBlack1.notLeaving
pitchBlack1.getSouthEast  = pitchBlack1.notLeaving
pitchBlack1.getNorthWest = pitchBlack1.notLeaving
pitchBlack1.getSouthWest  = pitchBlack1.notLeaving



function pitchBlack1.getSouth()

    return "kitchen"

end

return pitchBlack1
