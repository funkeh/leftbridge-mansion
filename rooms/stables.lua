-- Get house description
-- Stables: Location

local stables = {
    visited = false,
    items = {
        shovel = true,
        stables = "building2"
    }
}

-- Get Stable Description
function stables.onLook(fullDescription)

    local description = "The burnt out building frame provides no shelter from the rain."

    if fullDescription or not rooms.stables.visited then

        rooms.stables.visited = true

        description = "The wooden frame of the building sits rotting by the forest. " ..
        "Patchy remnants of the roof provide some shelter, but a gale whips up rain " ..
        "and goes straight through your clothing."

    end

    if roomItemCheck("shovel") then

        description = description .. " There is a shovel on the ground."

    end

    return description

end

-- Go South West
function stables.getSouthWest()

    return "houseSouth"

end


return stables
