-- Upstairs Foyer (outside)


local upstairsFoyer = {
    visited = false,
    items = { }
}


-- Get house description
function upstairsFoyer.onLook(fullDescription)

    local description= "You are standing in a dimly lit hallway with "..
    "a staircase.\n"..
    "There is a dimly lit area to your North, a door to your South, and a "..
    "staircase that leads down."

    if fullDescription or not upstairsFoyer.visited then

        upstairsFoyer.visited = true

        description = "You can barely make out anything in the dimlight of the room ,"..
        "but what you do see doesn't fill you with much confidence. The floor boards "..
        "have been torn up in some places, leaving dark spaces in between the "..
        "supports. Above you, you can make out some natural light from the sky "..
        "above. There is a constant dripping noise.\n"..
        "There is a dimly lit area to your North, a door to your South, and a "..
        "staircase that leads down."

    end

    return description

end


-- Get north path

function upstairsFoyer.getDown()

    return "foyer"

end

function upstairsFoyer.getSouth()

    return "masterBedroom"

end

function upstairsFoyer.getNorth()

    return "secondHallway"

end


return upstairsFoyer
