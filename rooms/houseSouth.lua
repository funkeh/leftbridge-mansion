-- chikun :: 2015
-- South of House (oustide)


local houseSouth = {
    visited = false,
    items = {
        rock = true,
    }
}


-- Get house description
function houseSouth.onLook(fullDescription)

    local description = "The house stands before you."

    if fullDescription or not rooms.houseSouth.visited then

        rooms.houseSouth.visited = true

        description = "In front of you is a raggedy old house. " ..
        "The door is sercured by chains " ..
        "It's pissing you off."

    end

    if roomItemCheck("rock") then

        description = description .. " There is a rock on the ground."

    end

    return description

end



-- Get south path

function houseSouth.getNorth()
    if roomItemCheck("door") then
        push("The door is locked.")
    else
        return "foyer"

    end

end

function houseSouth.getSouth()

    return "forestPath"

end

function houseSouth.getNorthEast()

    return "stables"

end

return houseSouth
