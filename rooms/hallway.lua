-- Hallway

local hallway = {
    visited = false
}

function hallway.onLook(fullDescription)

    local description = "Hallway."

    return description

end

function hallway.getSouthWest()

    return "foyer"

end

function hallway.getNorthWest()

    return "sittingRoom"

end

function hallway.getEast()

    return "guestRoom1"

end

return hallway
