-- Games Room

local gamesRoom = {
    visted = false,
    items = {
        poolTable = "table",
        blanket = true,
        corpse = true,
        matches = true,
    },
    counter = 0
}

function gamesRoom.onEnter()

    rooms.gamesRoom.counter = 0

end

function gamesRoom.onLook(fullDescription)

    local description = "You are standing in a large room filled with tables, and " ..
        "games. There is a large billiard table in the centre of the room." ..
        "There is an exit to the South."

    if fullDescription or not rooms.gamesRoom.visited then

        rooms.gamesRoom.visited = true

        description = "Chess sets, billiard tables, and a dozen other games you" ..
        " don't recognise. The owner of this house must have really been into games, " ..
        "or else just liked entertaining. If it wasn't for the circumstances of " ..
        "your visit, you probably would have enjoyed it here."

    end

    return description

end

-- On picking up the matches, the Guardian will approach the
-- the room. Wthin three turns, you will die unless you hide.

function gamesRoom.onInput(text)

    if matchesTaken then

        gamesRoom.notLeaving()

        return true

    end

    return false

end


function gamesRoom.notLeaving()

    if rooms.gamesRoom.counter == 0 then

        push("You hear a whirring sound in the hallway.")

    elseif rooms.gamesRoom.counter == 1 then

        push("Something is scratching against the door.")

    elseif rooms.gamesRoom.counter == 2 then

        push("Robotic, spidery limbs burst through the"..
            " the door. You can just barely make out a"..
            " blank face between the spinning blades"..
            " and claws.")

    elseif rooms.gamesRoom.counter == 3 then

        push("You have been torn to shreds.")

    else

        end

    rooms.gamesRoom.counter = rooms.gamesRoom.counter + 1

end


function gamesRoom.getSouth()

    return "secondHallway"

end


return gamesRoom
