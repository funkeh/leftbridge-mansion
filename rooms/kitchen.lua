--Room description

local kitchen = {
    visited = false,
    items = {
        knife = true,
        lantern = true,
        matches = true,
        oil = true
    }
}


-- Get house description
function kitchen.onLook(fullDescription)

    local description = "You are standing in a kitchen.\n"..
    "A large window sits to the South, a pitch black room to the North, and a dimly"..
    "lit room to the East."

    if fullDescription or not rooms.kitchen.visited then

        rooms.kitchen.visited = true

        description = "Cupboards and cabinets make up most of the room, with a long "..
        "bench running along the Western wall.\n"..
        "A large window sits to the South, a pitch black room to the North, and a"..
        "dimly lit room to the East."

    end

    if roomItemCheck("knife") then

        description = description .. " There is a knife on the bench."

    end

    if roomItemCheck("lantern") then

        description = description .. "There is a small"..
        " brass lantern flickering on the bench."

    end

    return description

end


-- Get

function kitchen.getEast()

    return "foyer"

end

-- Go to pitch black room unless you have a lantern.

function kitchen.getNorth()

    if invCheck("lantern") and lanternLit then

        return "chamber"

    else

        return "pitchBlack1"

    end

end

return kitchen
