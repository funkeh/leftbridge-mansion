-- Foyer (outside)


local foyer = {
    visited = false,
    items = { }
}


-- Get house description
function foyer.onLook(fullDescription)

    local description= "You are standing in a dimly lit room empty but for a rug "..
    "a staircase leading to the 2nd floor. "..
    "There's a light to the West, and two pitch black rooms to the North and East."

    if fullDescription or not rooms.foyer.visited then

        rooms.foyer.visited = true

        description = "You are standing in a room empty but for a staircase, and "..
        "a truly ancient rug that squelches when you step on it. "..
        "There's a light to the West, and pitch black room's to the North and East."

    end

    return description

end


-- Get to other rooms
function foyer.getSouth()

    return "houseSouth"

end

function foyer.getWest()

    return "kitchen"

end

function foyer.getUp()

    return "upstairsFoyer"

end

-- Get to Darkened Rooms

function foyer.getEast()

     if invCheck("lantern") then

        return "hallway"

    else

        return "pitchBlack2"

    end

end

function foyer.getNorth()

    if invCheck("lantern") and lanternLit then

        return "sittingRoom"

    else

        return "pitchBlack2"

    end

end

return foyer
