-- Study

local study = {
    visted = false,
}

function study.onLook(FullDescription)

    local description = "A large desk and bookshelf."

    if fullDescription or not rooms.study.visited then

        rooms.study.visited = true

        description = "The desk"..
    "commands respect, an ancient stained wood that looks like its made of mahogany."..
    " A pile of letters sits on the desk, and a drawer lies half open. The bookshelf"..
    " is small and only contains a few, ineligible volumes."

    return description

    end

end

function study.getSouth()

        return "secondHallway"

end

return study
