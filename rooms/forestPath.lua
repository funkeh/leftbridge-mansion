-- chikun :: 2015
-- Forest path (outside)


local forestPath = {
    visited = false,
    items = { }
}


-- Get house description
function forestPath.onLook(fullDescription)

    local description = "You are standing in a forest path."

    if fullDescription or not rooms.forestPath.visited then

        rooms.forestPath.visited = true

        description = description ..
        " To the north, you see a large house. To the south, the crash site."

    end

    return description

end


-- Get north path
function forestPath.getNorth()

    return "houseSouth"

end


-- Get south path
function forestPath.getSouth()

    return "crashSite"

end


return forestPath
