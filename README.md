# Leftbridge Mansion
## by Jane Slade & Josef Frank

A text-based game about demons and a car crash.

## Resources

* November font sourced from [Tepid Monkey Fonts](http://www.tepidmonkey.net/)