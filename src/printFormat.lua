-- Only prints left aligned
printFormat = function(text, x, y, limit)

    love.graphics.setColor(cols[1])

    text = text:gsub("\n", " nl ")

    originalX = x
    h = fnt:getHeight()
    space = fnt:getWidth(" ")

    for word in string.gmatch(text, "%S+") do

        if word:find("col=") then

            love.graphics.setColor(cols[tonumber(word:sub(5, -1))])

        elseif word:find("nl") then

            x = originalX
            y = y + h + fnt:getLineHeight()

        else

            local w = fnt:getWidth(word)

            if (x + w > originalX + limit) then

                x = originalX
                y = y + h + fnt:getLineHeight()

            end

            love.graphics.print(word, x, y)

            x = x + w + space

        end

    end

end
