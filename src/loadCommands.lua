-- chikun :: 2015
-- Loads all possible commands


cmd = {
    climb   = require "cmd/climb",
    hit     = require "cmd/hit",
    jump    = require "cmd/jump",
    look    = require "cmd/look",
    open    = require "cmd/open",
    quit    = require "cmd/quit",
    take    = require "cmd/take",
    use     = require "cmd/use",
    pull    = require "cmd/pull"
}
