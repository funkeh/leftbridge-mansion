-- chikun :: 2015
-- Lua game engine functions


-- Load libraries
require "src/loadCommands"  -- In-game commands
require "src/loadItems"     -- In-game items
require "src/loadRooms"     -- In-game rooms
require "src/loadSounds"    -- In-game sounds
require "src/runInput"      -- Checks input


push = push or print    -- Function for printing


room = "crashSite"      -- Room at which we start


-- Print description of current room
push(rooms[room].onLook())


-- Start up rain effect
playSound("rainOutside")


-- Check if any text starts with a certain term
function startsWith(text, check)

    -- Tabularise the inout
    local x = {text:find(check)}

    if #x == 0 or text:len() < check:len() then

        return(false)

    end

    if (text:len() == check:len()) or
       (text:sub(x[2] + 1, x[2] + 1) == " ") then
        return(x[1] == 1)
    end

    return(false)

end
