rooms = {
    crashSite  = require "rooms/crashSite",
    forestPath = require "rooms/forestPath",
    houseSouth = require "rooms/houseSouth",
    stables = require "rooms/stables",
    foyer = require "rooms/foyer",
    kitchen = require "rooms/kitchen",
    pitchBlack1 = require "rooms/pitchBlack1",
    pitchBlack2 = require "rooms/pitchBlack2",
    upstairsFoyer = require "rooms/upstairsFoyer",
    masterBedroom = require "rooms/masterBedroom",
    walkInWardrobe = require "rooms/walkInWardrobe",
    hallway = require "rooms/hallway",
    secondHallway = require "rooms/secondHallway",
    gamesRoom = require "rooms/gamesRoom",
    study = require "rooms/study",
    guestRoom1 = require "rooms/guestRoom1",
    guestRoom2 = require "rooms/guestRoom2",
    library = require "rooms/library",
    chamber = require "rooms/chamber",
    sittingRoom = require "rooms/sittingRoom"
}


function roomItemCheck(item)

    local test = rooms[room].items[item]

    if test == true then

        test = item

    end

    return test

end


function roomItemRemove(item)

    rooms[room].items[item] = nil

end
