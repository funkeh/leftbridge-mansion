local menu = { }

function menu:start()

    local themePos = {
        x = 277, y = 128
    }

    themeButtons = {
        {
            name = "Default",
            scheme = colSchemes.default,
            x = themePos.x, y = themePos.y,
            w = 100, h = 48
        },
        {
            name = "Inverted",
            scheme = colSchemes.inverted,
            x = themePos.x + 100, y = themePos.y,
            w = 100, h = 48
        },
        {
            name = "Matrix",
            scheme = colSchemes.matrix,
            x = themePos.x + 200, y = themePos.y,
            w = 100, h = 48
        },
        {
            name = "Solarised",
            scheme = colSchemes.solar,
            x = themePos.x, y = themePos.y + 48,
            w = 100, h = 48
        },
        {
            name = "Mocha",
            scheme = colSchemes.choc,
            x = themePos.x + 100, y = themePos.y + 48,
            w = 100, h = 48
        },
        {
            name = "Hotdog",
            scheme = colSchemes.hotdog,
            x = themePos.x + 200, y = themePos.y + 48,
            w = 100, h = 48
        }
    }

    fontButtons = {
        {
            name = "November",
            font = fntList.november,
            x = themePos.x, y = themePos.y+ 128,
            w = 100, h = 48
        },
        {
            name = "Roboto\nSlab",
            font = fntList.robotoslab,
            x = themePos.x + 100, y = themePos.y + 128,
            w = 100, h = 48
        },
        {
            name = "Terminus",
            font = fntList.terminus,
            x = themePos.x + 200, y = themePos.y + 128,
            w = 100, h = 48
        }
    }

end

function menu:update(dt)

end


function menu:onClick(x, y, mb)

    cursor = {
        x = x, y = y,
        w = 1, h = 1
    }

    for key, value in ipairs(themeButtons) do

        if math.overlap(cursor, value) then

            cols = value.scheme

        end

    end

    for key, value in ipairs(fontButtons) do

        if math.overlap(cursor, value) then

            fnt = value.font

        end

    end

end


function menu:onKey(key)

    if key == "escape" then

        menuOpen = false

    end

end

function menu:draw()

    love.graphics.setFont(fnt)

    for key, value in ipairs(themeButtons) do

        love.graphics.setColor(value.scheme[6])

        love.graphics.rectangle('fill', value.x, value.y, value.w, value.h)

        love.graphics.setColor(value.scheme[5])

        love.graphics.printf(value.name, value.x,
            value.y + value.h / 2 - love.graphics.getFont():getHeight() / 2,
            value.w, 'center')

        love.graphics.setColor(value.scheme[1])

        love.graphics.rectangle('line', value.x, value.y, value.w, value.h)

    end

    for key, value in ipairs(fontButtons) do

        love.graphics.setFont(value.font)

        love.graphics.setColor(cols[6])

        love.graphics.rectangle('fill', value.x, value.y, value.w, value.h)

        love.graphics.setColor(cols[5])

        fh = love.graphics.getFont():getHeight()

        if value.name == "Roboto\nSlab" then

            fh = fh * 2 + value.font:getLineHeight()

        end

        love.graphics.printf(value.name, value.x,
            value.y + value.h / 2 - fh / 2,
            value.w, 'center')

        love.graphics.rectangle('line', value.x, value.y, value.w, value.h)

    end

end

return menu

