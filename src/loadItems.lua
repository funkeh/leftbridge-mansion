items = {
    cliff = require "items/cliff",
    rock =  require "items/rock",
    shovel = require "items/shovel",
    frontDoor = require "items/houseFrontDoor",
    car = require "items/car",
    backpack = require "items/backpack",
    knife = require "items/knife",
    leftbridge = require "items/leftbridge",
    lantern = require "items/lantern",
    tornJacket = require "items/tornJacket",
    poolTable = require "items/poolTable",
    blanket = require "items/blanket",
    oil = require "items/oil",
    matches = require "items/matches",
    corpse = require "items/corpse"
}

inventory = { }


-- Adds an item to the inventory
function invAdd(input, item)

    inventory[input] = item

end


-- Checks if an item is in the inventory
function invCheck(item)

    for key, value in pairs(inventory) do

        if value == item then

            return inventory[key]

        end
    end

    return false

end


-- Remove an item from the inventory
function invRemove(item)

    for key, value in ipairs(inventory) do

        if value == item then

            inventory[key] = nil

            return true

        end
    end

    return false

end
