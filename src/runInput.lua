function runInput(input)


    -- If no input, set it to an empty string
    input = input or ""



    -- Convert command to lower case
    input = input:lower()


    -- Remove spaces at the end
    while input:sub(-1) == " " do

        input = input:sub(1, -2)

    end

    local testVal = false

    if rooms[room].onInput then

        testVal = rooms[room].onInput(input)

    end

    -- Actually compare with functions

    if testVal then

        -- do nothing

    -- Look tests
    elseif startsWith(input, "look at") then

        cmd.look("look at", input:sub(9))

    elseif startsWith(input, "look") then

        cmd.look("look", input:sub(6))

    elseif startsWith(input, "check") then

        cmd.look("check", input:sub(7))

    elseif startsWith(input, "examine") then

        cmd.look("examine", input:sub(9))


    -- Read tests
    elseif startsWith(input, "read") then

        cmd.look("read", input:sub(6))



    -- Open tests
    elseif startsWith(input, "open") then

        cmd.open("open", input:sub(6))


    -- Pull tests

    elseif startsWith(input, "pull") then

        cmd.pull("pull", input:sub(6))

    elseif startsWith(input, "push") then

        cmd.pull("push", input:sub(6))




    -- Use tests
    elseif startsWith(input, "use") then

        cmd.use("use", input:sub(5))



    -- Hit tests
    elseif startsWith(input, "hit") then

        cmd.hit("hit", input:sub(5))

    elseif startsWith(input, "break") then

        cmd.hit("break", input:sub(7))



    -- Jump tests
    elseif startsWith(input, "jump at") or startsWith(input, "jump on") or
       startsWith(input, "jump to") then

        cmd.jump("jump at", input:sub(9))

    elseif startsWith(input, "jump") then

        cmd.jump("jump at", input:sub(6))



    -- Climb tests
    elseif startsWith(input, "climb up") then

        cmd.climb("climb up", input:sub(10))

    elseif startsWith(input, "climb down") then

        cmd.climb("climb down", input:sub(12))

    elseif startsWith(input, "climb") then

        cmd.climb("climb", input:sub(7))



    -- Climb tests
    elseif startsWith(input, "pick up") or startsWith(input, "collect") then

        cmd.take("pick up", input:sub(9))

    elseif startsWith(input, "take") then

        cmd.take("take", input:sub(6))



    -- Game quit tests
    elseif startsWith(input, "quit") then

        cmd.quit("quit", input:sub(6))

    elseif startsWith(input, "exit") then

        cmd.quit("exit", input:sub(6))



    -- Angle movements
    elseif startsWith(input, "ne") or startsWith(input, "northeast") or
        startsWith(input, "go northeast") or startsWith(input, "goto northeast") or
        startsWith(input, "go to northeast") then

        local destination = rooms[room].getNorthEast

        if destination then

            local nextRoom = destination()

            if nextRoom then

                room = nextRoom

                push(rooms[room].onLook())

            end

        else

            push("The path is blocked to the north east.")

        end
    elseif startsWith(input, "nw") or startsWith(input, "northwest") or
        startsWith(input, "go northwest") or startsWith(input, "goto northwest") or
        startsWith(input, "go to northwest") then

        local destination = rooms[room].getNorthWest

        if destination then

            local nextRoom = destination()

            if nextRoom then

                room = nextRoom

                push(rooms[room].onLook())

            end

        else

            push("The path is blocked to the north west.")

        end
    elseif startsWith(input, "sw") or startsWith(input, "southwest") or
        startsWith(input, "go southwest") or startsWith(input, "goto southwest") or
        startsWith(input, "go to southwest") then

        local destination = rooms[room].getSouthWest

        if destination then

            local nextRoom = destination()

            if nextRoom then

                room = nextRoom

                push(rooms[room].onLook())

            end

        else

            push("The path is blocked to the south west.")

        end
    elseif startsWith(input, "se") or startsWith(input, "southeast") or
        startsWith(input, "go southeast") or startsWith(input, "goto southeast") or
        startsWith(input, "go to southeast") then

        local destination = rooms[room].getSouthEast

        if destination then

            local nextRoom = destination()

            if nextRoom then

                room = nextRoom

                push(rooms[room].onLook())

            end

        else

            push("The path is blocked to the south east.")

        end


    -- Movement
    elseif startsWith(input, "n") or startsWith(input, "north") or
        startsWith(input, "go north") or startsWith(input, "goto north") or
        startsWith(input, "go to north") then

        local destination = rooms[room].getNorth

        if destination then

            local nextRoom = destination()

            if nextRoom then

                room = nextRoom

                push(rooms[room].onLook())

            end

        else

            push("The path is blocked to the north.")

        end

    elseif startsWith(input, "s") or startsWith(input, "south") or
        startsWith(input, "go south") or startsWith(input, "goto south") or
        startsWith(input, "go to south") then

        local destination = rooms[room].getSouth

        if destination then

            local nextRoom = destination()

            if nextRoom then

                room = nextRoom

                push(rooms[room].onLook())

            end

        else

            push("The path is blocked to the south.")

        end

    elseif startsWith(input, "w") or startsWith(input, "west") or
        startsWith(input, "go west") or startsWith(input, "goto west") or
        startsWith(input, "go to west") then

        local destination = rooms[room].getWest

        if destination then

            local nextRoom = destination()

            if nextRoom then

                room = nextRoom

                push(rooms[room].onLook())

            end

        else

            push("The path is blocked to the west.")

        end

    elseif startsWith(input, "e") or startsWith(input, "east") or
        startsWith(input, "go east") or startsWith(input, "goto east") or
        startsWith(input, "go to east") then

        local destination = rooms[room].getEast

        if destination then

            local nextRoom = destination()

            if nextRoom then

                room = nextRoom

                push(rooms[room].onLook())

            end

        else

            push("The path is blocked to the east.")

        end

    elseif startsWith(input, "u") or startsWith(input, "up") or
        startsWith(input, "go up") or startsWith(input, "goto up") or
        startsWith(input, "go to up") then

        local destination = rooms[room].getUp

        if destination then

            room = destination()

            push(rooms[room].onLook())

        else

            push("There is no way to go up.")

        end

    elseif startsWith(input, "d") or startsWith(input, "down") or
        startsWith(input, "go down") or startsWith(input, "goto down") or
        startsWith(input, "go to down") then

        local destination = rooms[room].getDown

        if destination then

            room = destination()

            push(rooms[room].onLook())

        else

            push("There is no way to go down.")

        end



    -- Easter eggs
    elseif input == "yes" then

        push("No.")

    elseif input == "no" then

        push("Yes.")



    -- If we don't know what they mean
    else

        push("You don't know how to " .. input .. ".")

    end

    if room ~= prevRoom then

        prevRoom = room

        if rooms[room].onEnter then

            rooms[room].onEnter()

        end

    end

end
