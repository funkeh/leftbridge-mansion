-- Load all audio if can
if love then

    audio = {
        rainOutside    = love.audio.newSource('ogg/rainOutside.ogg', 'stream')
    }

    -- Loop certain sounds
    audio.rainOutside:setLooping(true)

end



-- Sound functions

-- Plays a sound if in love version of game
function playSound(sound)

    if love then

        audio[sound]:play()

    end
end

-- Stops a sound if in love version of game
function stopSound(sound)

    if love then

        audio[sound]:stop()

    end
end
