-- chikun :: 2015
-- Lua text based engine


-- Get player name
while (name == nil) do

    io.write("What is your name?\n> ")
    local newName = io.read()

    io.write("Would you like to name your character '" .. newName .. "'?\n>")
    local decision = io.read()

    if decision == "y" or decision == "yes" then

        name = newName

    end
end


push = function(text)

    for word in string.gmatch(text, "%S+") do

        if not word:find("col=") then

            io.write(word .. " ")

        end

    end

    io.write("\n")

end

require "src/game"      -- Load up game variables and functions


loop = true

while loop do

    io.write("> ")
    runInput(io.read())

end
