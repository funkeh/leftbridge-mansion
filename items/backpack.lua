local backpack = {
    name = "Backpack"
}


function backpack.onLook()

    push("Wet, but undamaged. You can probably fit a lot of stuff in here.")

end


function backpack.onTake()

    push("You slip pack onto your back.")

end


return backpack
