local corpse = {
    name = "Corpse"
}

function corpse.onLook()

    if blanketGone then

        description = "The corpse's skin has been dried and "..
        "and stretched over its bones. Not a trace of fat or "..
        "flesh left there, as though something has sucked it dry."..
        "Something is bulging in its pockets."

        return description

    else

        push("You cannot see a corpse.")

    end

end

return corpse
