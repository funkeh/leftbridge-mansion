local cliff = {
    name = "Cliff"
}


function cliff.onLook()

    push("At least thirty feet and soaked in water, there is no way you will be able to climb it. " ..
    "\nAt the top of the cliff are a set of twisted and broken rails.")

end


function cliff.onClimb()

    push("You climbed the cliff.")

end


return cliff
