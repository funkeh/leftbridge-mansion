local rock = {
    name = "Rock"
}


function rock.onLook()

    push("Just your average rock.")

end


function rock.onTake()

    push("You picked up the rock. It feels good in your hand.")

end


return rock
