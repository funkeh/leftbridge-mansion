local tornJacket = {
    name = "Coat"
}


function tornJacket.onLook()

    push ("Great chunks of the coat have been torn away "..
        " from something like a rusted dagger. There is "..
        "little to salvage here. The coat has been stained "..
        "red.")

end


function tornJacket.onTake()

    push("You put away the torn coat.")

end


return tornJacket
