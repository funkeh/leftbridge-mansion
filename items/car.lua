local car = {
    name = "Car"
}


function car.onLook()

    push("The car has been flipped and torn as it crashed against the forest floor."..
    "Already water has pooled on what was once the ceiling. Floating serenely through "..
    "the water is your backpack.")

end


function car.onClimb()

    push("You are standing on a car.")

end


return car
