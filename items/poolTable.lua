local poolTable = {
    name = "Billiards Table"
}

function poolTable.onLook()

    push ("The billiards table is half hidden by "..
    "a blanket that now hangs half over the side and over "..
    "something on the floor. You didn't even realise they "..
    "played billiards years ago. If you have to stay the "..
    "night, maybe you can play some games.")

end

return poolTable
