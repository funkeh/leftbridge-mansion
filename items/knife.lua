local knife = {
    name = "Knife"
}


function knife.onLook()

    push("It is a large butcher knife.")

end


function knife.onTake()

    push("You picked up the knife. It feels good in your hand.")

end


return knife
