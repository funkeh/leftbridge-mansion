local matches = {
    name = "matches"
}

function matches.onLook(description)

    description = "A box of matches."

    return description

end

function matches.onTake()

    push("You pick up the matches.")

    matchesTaken = true

    push("You begin to hear a faint whirring sound, like"..
        " the fan on a computer, coming from somewhere close"..
        " by.")

    return true

end



function matches.onUse(object)

    if object == "" then

        push("What would you like to use matches on.")

    elseif object == "lantern" and invCheck("lantern") then

        if lanternFilled then

            push("You light the lantern.")

            lanternLit = true

            return true

        else

            push("You attempt to light wick, but to no avail.")

        end

    else

        push("You shouldn't light " .. object .. " on fire.")

    end

end


return matches
