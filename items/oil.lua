local oil = {
    name = "oil"
}

function oil.onLook(description)

    description = "A cannister of oil."

    return description

end

function oil.onTake()

    push("It sloshes when you pick it up.")

end

function oil.onUse(object)

    if object == "" then

        push("What would you like to use oil on?")

    elseif object == "lantern" and invCheck("lantern") then

        lanternFilled = true

        invRemove("oil")

        push("You pour oil into the lantern.")

        return true

    else

        push("You shouldn't pour oil on " .. object .. ".")

    end

end

return oil
