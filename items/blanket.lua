local blanket = {
    name = "Blanket"
}

function blanket.onLook()

        push ("A musty old blanket covering something "..
        "underneath the table.")

end

function blanket.onPull()

    blanketGone = true

    push ("The blanket falls down, and you jump back "..
        "your heart pounding. Lying beneath the blanket, "..
        "their face twisted in a contoured grimace is a "..
        "mummified corpse.")

    return true

end

return blanket
