local frontDoor = {
    name = "Door"
}

function frontDoor.onLook()

    push("A large black door that has been secured by steel chains.")

end

return frontDoor
