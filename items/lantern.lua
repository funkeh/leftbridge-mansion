local lantern = {
    name = "Lantern"
}


function lantern.onLook()

    push("It is a ornate silver lantern.")

end

function lantern.onTake()

    push("You pick up the lantern.")

end

return lantern
