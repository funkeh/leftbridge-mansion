local shovel = {
    name = "Shovel"
}

function shovel.onLook()

    push("It is a rusty shovel.")

end

function shovel.onTake()

    push("The shovel feels sturdy. It could come in handy.")

end

function shovel.onHit(object)

    if object == "door" and roomItemCheck("door") then

        roomItemRemove("door")

        push("You use the shovel to destroy the lock on the door.")

        return true

    end

end


function shovel.onUse(object)

    if object == "door" and roomItemCheck("door") then

        items.shovel.onHit("door")

        return true

    end

end


return shovel
