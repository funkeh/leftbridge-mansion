local climb = function(command, input)

    if input == "" then

        -- Ask player what to climb
        push("What will you climb?")

    else

        local itemCheck = roomItemCheck(input)

        if itemCheck then

            if items[itemCheck].onClimb then

                items[itemCheck].onClimb()

                return true

            end

        end

        push("You cannot " .. command .. " " .. input .. ".")

    end

end


return(climb)
