local look = function(command, input)

    if input == "" then

        -- Print the room description
        push(rooms[room].onLook(true))

    else

        local itemCheck = roomItemCheck(input)

        if itemCheck then

            if items[itemCheck].onLook then

                items[itemCheck].onLook()

                return true

            end

        end

        push("You cannot see a " .. input .. ".")

    end
end


return(look)
