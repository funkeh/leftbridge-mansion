local jump = function(command, input)

    if input == "" then

        -- Ask player what to jump at
        push("What will you jump at?")

    else

        local itemCheck = roomItemCheck(input)

        if itemCheck then

            if items[itemCheck].onJump then

                items[itemCheck].onJump()

                return true

            end

        end

        push("You cannot " .. command .. " " .. input .. ".")

    end

end


return(jump)
