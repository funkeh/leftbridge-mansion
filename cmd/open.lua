local open = function(command, input)

    if input == "" then

        -- Ask player what to use
        push("What will you " .. command .."?")

    else

        local with, useObject, text =
            input:find(" with "),
            "",
            input

        if input:find(" ") then

            useObject = input:sub(input:find(" ") + 1, -1)

            text = input:sub(1, input:find(" ") - 1)

        end

        if with then

            useObject = input:sub(with + 6)

            text = input:sub(1, with - 1)

        end

        local itemCheck = roomItemCheck(text)

        if itemCheck then

            if items[itemCheck].onOpen then

                if items[itemCheck].onOpen(useObject) then

                    return true

                else

                    push("You cannot " .. command .. " the " .. text ..
                        " with the " .. useObject ..".")

                    return false

                end

            else

                push("You cannot " .. command .. " the " .. text .. ".")

                return false

            end

        end

        push("You have no " .. text .. " to " .. command .. ".")

    end

end


return(open)
