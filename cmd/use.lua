local use = function(command, input)

    if input == "" then

        -- Ask player what to use
        push("What will you " .. command .."?")

    else

        local on, with, useObject, text =
            input:find(" on "),
            input:find(" with "),
            "",
            input

        if input:find(" ") then

            useObject = input:sub(input:find(" ") + 1, -1)

            text = input:sub(1, input:find(" ") - 1)

        end

        if on then

            useObject = input:sub(on + 4)

            text = input:sub(1, on - 1)

        elseif with then

            useObject = input:sub(with + 6)

            text = input:sub(1, with - 1)

        end

        local itemCheck = invCheck(text)

        if itemCheck then

            if items[itemCheck].onUse then

                if items[itemCheck].onUse(useObject) then

                    return true

                else

                    push("You cannot " .. command .. " the " .. text ..
                        " on the " .. useObject ..".")

                    return false

                end

            else

                push("You cannot " .. command .. " the " .. text .. ".")

                return false

            end

        end

        push("You have no " .. text .. " to " .. command .. ".")

    end

end


return(use)
