local take = function(command, input)

    if input == "" then

        -- Ask player what to take
        push("What will you " .. command .."?")

    else

        local itemCheck = roomItemCheck(input)

        if itemCheck then

            if items[itemCheck].onTake then

                invAdd(input, itemCheck)

                roomItemRemove(input)

                items[itemCheck].onTake()

                return true

            else

                push("You cannot " .. command .. " the " .. input .. ".")

                return false

            end

        end

        push("There is no " .. input .. " to " .. command .. ".")

    end

end


return(take)
